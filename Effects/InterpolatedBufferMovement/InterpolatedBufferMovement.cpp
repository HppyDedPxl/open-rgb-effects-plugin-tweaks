#include "InterpolatedBufferMovement.h"
#include "ColorUtils.h"

REGISTER_EFFECT(InterpolatedBufferMovement)

InterpolatedBufferMovement::InterpolatedBufferMovement(QWidget* parent):
    RGBEffect(parent),
    ui(new Ui::InterpolatedBufferMovement)
{
    ui->setupUi(this);

    currentStepInterpolator = 0.0;
    EffectDetails.EffectName = "InterpolatedBufferMovement";
    EffectDetails.EffectClassName = ClassName();
    EffectDetails.EffectDescription = "Shift the Color Buffer across a Map- interpolating it's colors smoothly. Very Similar to Custom Marque but with blending";
    EffectDetails.IsReversable = true;
    EffectDetails.MaxSpeed     = 200;
    EffectDetails.MinSpeed     = 1;
    EffectDetails.HasCustomSettings = true;

    ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->colors->setLayout(new QHBoxLayout());
    ui->colors->layout()->setSizeConstraint(QLayout::SetFixedSize);
    ui->scrollArea->setWidgetResizable(true);

    SetSpeed(25);

    ResetColors();
}

InterpolatedBufferMovement::~InterpolatedBufferMovement(){
    delete ui;
}

void InterpolatedBufferMovement::DefineExtraOptions(QLayout* layout)
{
    layout->addWidget(this);
}

void InterpolatedBufferMovement::ResetColors()
{
    QLayoutItem *child;

    currentStepInterpolator = 0.0;
    while ((child = ui->colors->layout()->takeAt(0)) != 0) {
        delete child->widget();
    }

    unsigned int colors_count = ui->colors_count_spinBox->value();

    color_pickers.resize(colors_count);
    colors.resize(colors_count);

    for(size_t i = 0; i < colors_count; i++)
    {
        ColorPicker* picker = CreatePicker(i);
        ui->colors->layout()->addWidget(picker);
    }

}


ColorPicker* InterpolatedBufferMovement::CreatePicker(int i)
{
    ColorPicker* picker = new ColorPicker();
    picker->SetRGBColor(colors[i]);

    color_pickers[i] = picker;

    connect(picker, &ColorPicker::ColorSelected, [=](QColor c){
        colors[i] = ColorUtils::fromQColor(c);
    });

    return picker;
}

void InterpolatedBufferMovement::on_colors_count_spinBox_valueChanged(int)
{
    ResetColors();
}





void InterpolatedBufferMovement::LoadCustomSettings(json settings)
{
    if (settings.contains("colors"))
    {
        colors.clear();

        for(size_t color : settings["colors"])
        {
            colors.push_back(color);
        }

        ui->colors_count_spinBox->setValue(colors.size());
    }

    ResetColors();
}

json InterpolatedBufferMovement::SaveCustomSettings(json settings)
{
    settings["colors"] = colors;
    return settings;
}


RGBColor InterpolatedBufferMovement::GetColorForLed(unsigned int idx){

    int offset = (int)currentStepInterpolator;
    double interpolator = currentStepInterpolator - offset;

    int startColIdx = (idx + (offset - 1)) % colors.size();
    int nextColIdx = (startColIdx + 1) % colors.size();


    
    //new hsv lerping
    //  hsv_t startHsv;
    //  rgb2hsv(colors[startColIdx], &startHsv);
    //  hsv_t endHsv;
    //  rgb2hsv(colors[nextColIdx], &endHsv);

    //  hsv_t newHsv;
    //  newHsv.saturation =  (float)Clamp(startHsv.saturation + ((endHsv.saturation - startHsv.saturation) * interpolator),0.0,255.0);
    //  newHsv.hue = (float)Clamp(startHsv.hue + ((endHsv.hue - startHsv.hue) * interpolator),0.0,360.0);
    //  newHsv.value = (float)Clamp(startHsv.value + ((endHsv.value - startHsv.value) * interpolator),0.0,255.0);

    //  return hsv2rgb(&newHsv);


    // old rgb lerping
    int startCol[3] = {
        RGBGetRValue(colors[startColIdx]),
        RGBGetGValue(colors[startColIdx]),
        RGBGetBValue(colors[startColIdx])
    };
    int nextCol[3] = {
        RGBGetRValue(colors[nextColIdx]),
        RGBGetGValue(colors[nextColIdx]),
        RGBGetBValue(colors[nextColIdx])
    };
 
    int finalCol[3];

    int step = 0;
    for (int c = 0; c < 3 ; c++ ) {
        step = nextCol[c] - startCol[c];
        finalCol[c] = int(startCol[c] + (step*(interpolator)));
    }

    return ToRGBColor(finalCol[0],finalCol[1],finalCol[2]);
}

double InterpolatedBufferMovement::Clamp(double v, double lo, double hi){
    return v < lo ? lo : (v > hi ? hi : v);
}

void InterpolatedBufferMovement::StepEffect(std::vector<ControllerZone*> controller_zones)
{
    for(unsigned int i = 0; i < controller_zones.size(); i++)
    {
        int start_idx = controller_zones[i]->start_idx();
        zone_type ZT = controller_zones[i]->type();
        int leds_count = controller_zones[i]->leds_count();
        bool reverse = controller_zones[i]->reverse;

        if (ZT == ZONE_TYPE_SINGLE || ZT == ZONE_TYPE_LINEAR)
        {
            for (int LedID = 0; LedID < leds_count; LedID++)
            {
                RGBColor color = GetColorForLed(reverse ? leds_count - LedID - 1 : LedID);
                controller_zones[i]->SetLED(start_idx + LedID, color, Brightness);
            }
        }

        else if (ZT == ZONE_TYPE_MATRIX)
        {
            int cols = controller_zones[i]->matrix_map_width();
            int rows = controller_zones[i]->matrix_map_height();

            for (int col_id = 0; col_id < cols; col_id++)
            {
                RGBColor color = GetColorForLed(reverse ? cols - col_id - 1: col_id);

                for (int row_id = 0; row_id < rows; row_id++)
                {
                    int LedID = controller_zones[i]->controller->zones[controller_zones[i]->zone_idx].matrix_map->map[((row_id * cols) + col_id)];
                    controller_zones[i]->SetLED(start_idx + LedID, color, Brightness);
                }
            }

        }
    }

    double step = ((Speed / (double) (FPS * 4.0)));
    currentStepInterpolator += step;
}
