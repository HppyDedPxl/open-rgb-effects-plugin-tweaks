#ifndef INTERPOLATEDBUFFERMOVEMENT_H
#define INTERPOLATEDBUFFERMOVEMENT_H

/* Class heavily draws from the Custom Marquee effect */


#include <QWidget>
#include "ui_InterpolatedBufferMovement.h"
#include "RGBEffect.h"
#include "EffectRegisterer.h"
#include "hsv.h"
#include "ColorPicker.h"



namespace Ui {
class InterpolatedBufferMovement;
}

class InterpolatedBufferMovement : public RGBEffect
{
    Q_OBJECT

public:
    explicit InterpolatedBufferMovement(QWidget *parent = nullptr);
    ~InterpolatedBufferMovement();

    EFFECT_REGISTERER(ClassName(), CAT_ADVANCED, [](){return new InterpolatedBufferMovement;});

     static std::string const ClassName() {return "InterpolatedBufferMovement";}

     void DefineExtraOptions(QLayout*) override;
     void StepEffect(std::vector<ControllerZone*>) override;
     void LoadCustomSettings(json) override;
     json SaveCustomSettings(json) override;


private slots:
    void on_colors_count_spinBox_valueChanged(int);

private:
    Ui::InterpolatedBufferMovement *ui;
    RGBColor GetColorForLed(unsigned int);
    double Clamp(double v, double lo, double hi);
    void ResetColors();
    ColorPicker* CreatePicker(int);
    std::vector<RGBColor> colors;
    double currentStepInterpolator;
    std::vector<ColorPicker*> color_pickers;


};

#endif // INTERPOLATEDBUFFERMOVEMENT_H
